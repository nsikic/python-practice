import os

class moneyCounter:

    def dodaj100hrk():
        return {
            "valuta" : "HRK",
            "vrijednost": 100
            }

    def count(path = "C:/data"):
        files_to_read = os.listdir(path)
    
        print(f"Searching for money files in {path} \n ")

        currency_per_city = {}

        total_currencies = {}
        # otvaranje svih file-ova u folder-u
        for file in files_to_read:
            if os.path.isfile(os.path.join(path, file)):
                #otvaranje file-a
                f = open(os.path.join(path, file), 'r')
                print(f"{file} found: ")
                print(" Totals by currencies: ")
                #citanje file-a
                for x in f:
                    #parsiranje
                    parsed_row = x.split(", ")    

                    # stavljanje pojedinacnog grada u zajednicki dictonary
                    if parsed_row[1] not in total_currencies:
                        total_currencies[(parsed_row[1])] = int(parsed_row[2])
                    else:
                        total_currencies[(parsed_row[1])] += int(parsed_row[2])

                    # stavljanje pojedinacnog grada u dictonary
                    if parsed_row[1] not in currency_per_city:
                        currency_per_city[parsed_row[1]] = int(parsed_row[2])
                    else:
                        currency_per_city[parsed_row[1]] += int(parsed_row[2])
                        
                #ispis svakog pojedinacnog grada po valutama
                for currency in currency_per_city:
                    print(f"   {currency}: {currency_per_city[currency]} ")
                    
                print("")
                #ciscenje dictionary-a
                currency_per_city = {}
                f.close()

        print("Money in all countries: ")
        #ispis sume valuta i iznosa svih gradova
        for currency in total_currencies:
            print(f"        {currency}: {total_currencies[currency]}")
