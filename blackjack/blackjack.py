import random
from tkinter import Y
cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
cards_standard = cards

player_cards = []
dealer_cards = []

new_game_bool = False
start_game = False

req_start_game = input("Do you want to play a game of blackjack? ")


def draw_card(user):
    card_drawn = random.choice(cards)
    cards.remove(card_drawn)
    user.append(card_drawn)

def show_cards_hidden(user, cpu):
    print(f"Player: {player_cards}, {sum(player_cards)}  ")
    print(f"Dealer: [{dealer_cards[0]}, X], {dealer_cards[0]}  ")

def show_cards_reveal(user, cpu):
    print(f"Player: {player_cards}, {sum(player_cards)} ")
    print(f"Dealer: {dealer_cards}, {sum(dealer_cards)} \n")


def hit_lose(user_cards):
    if(sum(user_cards) > 21):
        solve_results(user_cards, dealer_cards)


def solve_results(user_cards, cpu_cards):
    if(sum(cpu_cards) > 21):
        print("Dealer bust, you won! ")
        play_again()

    elif sum(user_cards) > 21:
        print("Bust, you lost! ")
        play_again()

    elif( sum(user_cards) == sum(cpu_cards) ):
        print("Draw. ")
        play_again()

    elif(sum(user_cards) > sum(cpu_cards)):
        print("You won.")
        play_again()

    elif(sum(cpu_cards) > sum(user_cards)):
        print("You lost.")
        play_again()

def stand(user_cards, cpu_cards):
    while (sum(dealer_cards) < 17):
                draw_card(dealer_cards)
                show_cards_reveal(player_cards, dealer_cards)
    start_game = solve_results(player_cards, dealer_cards)
    return start_game
        

def input_hit_stand(hit_or_stand):

    if(hit_or_stand.capitalize() == "Hit"):
        draw_card(player_cards)
        show_cards_reveal(player_cards, dealer_cards)

    elif(players_next_move.capitalize() == "Stand"):
        show_cards_reveal(player_cards, dealer_cards)
        stand(player_cards, dealer_cards)
    


def check_aces(user_cards):
    if(sum(user_cards) > 21):
        for card in (len(user_cards), 1):
            if card == 11:
                card = 1

    if(sum(user_cards) > 21):
        hit_lose(user_cards)
        

        
def play_again():
    another_start_game = input("Do you want to play another game of blackjack? ").capitalize()
    if(another_start_game == "Y"):
        player_cards.clear()
        dealer_cards.clear()
        card = cards_standard
        new_game()
    else:
        quit_game()


def new_game():

    for start in range(0, 2, 1):
        draw_card(player_cards)    
        draw_card(dealer_cards)
        new_game_bool = False
        start_game = True
        

def quit_game():

    state = "quit"
    start_game = False
    print("Thanks for playing :) ")
    quit()



if(req_start_game == "y"):
    start_game = True
    new_game_bool = True
    new_game()
    state = "game"
else:
    quit_game()
   

while start_game:
    if state == "game":

        show_cards_hidden(player_cards, dealer_cards)

        if(sum(player_cards) < 22):
            players_next_move = input("Do you want to 'hit' or 'stand': ")
            input_hit_stand(players_next_move)
        else:
            check_aces(player_cards)

    
    if state == "post_game":

        play_again()



    



