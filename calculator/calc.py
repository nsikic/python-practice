
def add(n1, n2):
  return n1 + n2

def substract(n1, n2):
  return n1 - n2

def multiply(n1, n2):
  return n1 * n2

def divide(n1, n2):
  return n1 / n2

operations = {
  "+": add,
  "-": substract,
  "*": multiply,
  "/": divide
}

def calculator():

  num1 = float(input("Enter 1st number: "))
  calculate = True

  for operation in operations:
    print(operation)

  while calculate:
    user_operation = input("Pick operation: ")
    num2 = float(input("enter 2nd number: "))
    calc_func = operations[user_operation]
    answer = calc_func(num1, num2)
    print(f" {num1} {user_operation} {num2} = {answer} \n")

    if input(f""" Type 'y' to continue calculating with {answer},  or type 'n' to start a new calculation: """) == "y":
      num1 = answer
    else:
      calculate = False
      calculator()

